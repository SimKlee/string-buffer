# Laravel String Buffer

A small library to manipulate strings object orientated with fluent interface and method chaining.

## Installation

```shell
composer require simklee/string-buffer
```

## Usage

```php
use Simklee\LaravelStringBuffer\StringBuffer;

// Sample with append methods
$buffer = new StringBuffer();
$buffer->append('some string')
    ->appendIf($condition, 'true', 'false')
    ->appendIfNot($condition, 'false', 'true')
    ->appendFormatted('%s, %s', 'LastName', 'FirstName')
    ->appendIfNull($isNull, 'is null', 'is NOT null')
    ->appendIfNotNull($isNotNull, 'is NOT null', 'is null')
    ->appendImplode(['a', 'b', 'c'], ',');
echo $buffer->toString();

// Sample with prepend methods
echo StringBuffer::create('some string')
    ->prepend('some string')
    ->prependIf($condition, 'true', 'false')
    ->prependIfNot($condition, 'false', 'true')
    ->prependFormatted('%s, %s', 'LastName', 'FirstName')
    ->prependIfNull($isNull, 'is null', 'is NOT null')
    ->prependIfNotNull($isNotNull, 'is NOT null', 'is null')
    ->prependImplode(['a', 'b', 'c'], ',')
    ->toString();
```

## Methods

| Method                                                                                     | Description                                                                                 |
|--------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------|
| **__construct**(_string_ $string = null)                                                   | Constructor of StringBuffer                                                                 |
| StringBuffer _static_ **create**(_string_ $string = null)                                  | Creates an instance of StringBuffer. Good for method chaining                               |                      
| string **toString**()                                                                      | Get the string value of StringBuffer                                                        |
| string **__toString**()                                                                    | Get the string value of StringBuffer                                                        |
 | StringBuffer **append**(_string_ $string)                                                  | Appends a string to the buffer.                                                             |
 | StringBuffer **appendFormatted**(_string_ $format, _string_ ...$values)                    | Appends a formatted string (with _sprintf()_) to the buffer.                                |
 | StringBuffer **appendIf**(_bool_ $condition, _string_ $string, _string_ $else = null)      | Appends a string to the buffer if _$condition_ is _true_, otherwise _$else_ (if not null)   |
 | StringBuffer **appendIfNot**(_bool_ $condition, _string_ $string, _string_ $else = null)   | Appends a string to the buffer if _$condition_ is _false_, otherwise _$else_ (if not null)  |
| StringBuffer **appendIfNull**(_mixed_ $value, _string_ $string, _string_ $else = null)     | Appends a string to the buffer if _$value_ is _null_, otherwise _$else_ (if not null)       |
| StringBuffer **appendIfNotNull**(_mixed_ $value, _string_ $string, _string_ $else = null)  | Appends a string to the buffer if _$value_ is NOT _null_, otherwise _$else_ (if not null)   |
| StringBuffer **appendImplode**(_array_ $values, _string_ $separator = ' ')                 | Appends an imploded array to the buffer.                                                    |
 | StringBuffer **prepend**(_string_ $string)                                                 | Prepends a string to the buffer.                                                            |
 | StringBuffer **prependFormatted**(_string_ $format, _string_ ...$values)                   | Prepends a formatted string (with _sprintf()_) to the buffer.                               |
 | StringBuffer **prependIf**(_bool_ $condition, _string_ $string, _string_ $else = null)     | Prepends a string to the buffer if _$condition_ is _true_, otherwise _$else_ (if not null)  |
 | StringBuffer **prependIfNot**(_bool_ $condition, _string_ $string, _string_ $else = null)  | Prepends a string to the buffer if _$condition_ is _false_, otherwise _$else_ (if not null) |
| StringBuffer **prependIfNull**(_mixed_ $value, _string_ $string, _string_ $else = null)    | Prepends a string to the buffer if _$value_ is _null_, otherwise _$else_ (if not null)      |
| StringBuffer **prependIfNotNull**(_mixed_ $value, _string_ $string, _string_ $else = null) | Prepends a string to the buffer if _$value_ is NOT _null_, otherwise _$else_ (if not null)  |
| StringBuffer **prependImplode**(_array_ $values, _string_ $separator = ' ')                | Prepends an imploded array to the buffer.                                                   |
