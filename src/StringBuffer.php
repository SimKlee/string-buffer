<?php

declare(strict_types=1);

namespace SimKlee\StringBuffer;

use Illuminate\Support\Str;

class StringBuffer
{
    private string $string = '';

    public function __construct(string $string = null)
    {
        if (!is_null($string)) {
            $this->string = $string;
        }
    }

    public static function create(string $string = null): StringBuffer
    {
        return new StringBuffer($string);
    }

    public function append(string $string): StringBuffer
    {
        $this->string .= $string;

        return $this;
    }

    public function appendFormatted(string $format, string ...$values): StringBuffer
    {
        $this->append(sprintf($format, ...$values));

        return $this;
    }

    public function appendIf(bool $condition, string $string, string $else = null): StringBuffer
    {
        if ($condition) {
            $this->append($string);
        } elseif (!is_null($else)) {
            $this->append($else);
        }

        return $this;
    }

    public function appendIfNot(bool $condition, string $string, string $else = null): StringBuffer
    {
        return $this->appendIf(!$condition, $string, $else);
    }

    public function appendIfNull(mixed $value, string $string, string $else = null): StringBuffer
    {
        return $this->appendIf(is_null($value), $string, $else);
    }

    public function appendIfNotNull(mixed $value, string $string = null, string $else = null): StringBuffer
    {
        return $this->appendIf(!is_null($value), $string ?? $value, $else);
    }

    /**
     * @param array<string> $values
     */
    public function appendImplode(array $values, string $separator = ' ', string $prefix = null, string $suffix = null): StringBuffer
    {
        return $this->appendIfNotNull($prefix)
                    ->append(implode(separator: $separator, array: $values))
                    ->appendIfNotNull($suffix);
    }

    /**
     * @param array<string> $values
     */
    public function appendImplodeIfNotEmpty(array $values, string $separator = ' ', string $prefix = null, string $suffix = null): StringBuffer
    {
        if (count($values) > 0) {
            return $this->appendImplode($values, $separator, $prefix, $suffix);
        }

        return $this;
    }

    public function prepend(string $string): StringBuffer
    {
        $this->string = $string . $this->string;

        return $this;
    }

    public function prependFormatted(string $format, string ...$values): StringBuffer
    {
        $this->prepend(sprintf($format, ...$values));

        return $this;
    }

    public function prependIf(bool $condition, string $string, string $else = null): StringBuffer
    {
        if ($condition) {
            $this->prepend($string);
        } elseif (!is_null($else)) {
            $this->prepend($else);
        }

        return $this;
    }

    public function prependIfNot(bool $condition, string $string, string $else = null): StringBuffer
    {
        return $this->prependIf(!$condition, $string, $else);
    }

    public function prependIfNull(mixed $value, string $string, string $else = null): StringBuffer
    {
        return $this->prependIf(is_null($value), $string, $else);
    }

    public function prependIfNotNull(mixed $value, string $string, string $else = null): StringBuffer
    {
        return $this->prependIf(!is_null($value), $string, $else);
    }

    /**
     * @param array<string> $values
     */
    public function prependImplode(array $values, string $separator = ' '): StringBuffer
    {
        return $this->prepend(implode(separator: $separator, array: $values));
    }

    public function replace(array|string $search, array|string $replace = ''): StringBuffer
    {
        $this->string = Str::replace($search, $replace, $this->string);
        return $this;
    }

    public function substring(int $start, ?int $length = null): StringBuffer
    {
        $this->string = Str::substr($this->string, $start, $length);
        return $this;
    }

    public function toString(): string
    {
        return $this->string;
    }

    public function __toString(): string
    {
        return $this->string;
    }

    public function snake(): StringBuffer
    {
        $this->string = Str::snake($this->string);
        return $this;
    }

    public function lower(): StringBuffer
    {
        $this->string = Str::lower($this->string);
        return $this;
    }

    public function upper(): StringBuffer
    {
        $this->string = Str::upper($this->string);
        return $this;
    }

    public function remove(string $search, bool $caseSensitive = true): StringBuffer
    {
        $this->string = Str::remove(search: $search, subject: $this->string, caseSensitive: $caseSensitive);
        return $this;
    }

    public function camel(): StringBuffer
    {
        $this->string = Str::camel($this->string);
        return $this;
    }

    public function kebab(): StringBuffer
    {
        $this->string = Str::kebab($this->string);
        return $this;
    }

    public function upperFirst(): StringBuffer
    {
        $this->string = Str::ucfirst($this->string);
        return $this;
    }

    public function splitWords(): StringBuffer
    {
        return $this->snake()
                    ->replace('_', ' ');
    }

    public function upperWords(): StringBuffer
    {
        $this->string = ucwords($this->string);
        return $this;
    }

    public function lowerFirst(): StringBuffer
    {
        $this->string = lcfirst($this->string);
        return $this;
    }

    public function plural(): StringBuffer
    {
        $this->string = Str::plural($this->string);
        return $this;
    }

    public function singular(): StringBuffer
    {
        $this->string = Str::singular($this->string);
        return $this;
    }

    public function ascii(): StringBuffer
    {
        $this->string = Str::ascii($this->string);
        return $this;
    }

    public function limitWords(int $words, string $end = '...'): StringBuffer
    {
        $this->string = Str::words(value: $this->string, words: $words = 100, end: $end);
        return $this;
    }
}
