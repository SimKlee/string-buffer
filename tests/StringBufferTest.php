<?php

declare(strict_types=1);

namespace Simklee\LaravelStringBuffer\Tests;

use PHPUnit\Framework\TestCase;
use SimKlee\LaravelStringBuffer\StringBuffer;

class StringBufferTest extends TestCase
{
    public function testInstance()
    {
        $buffer = new StringBuffer('some string');
        $this->assertInstanceOf(StringBuffer::class, $buffer);
        $this->assertSame($buffer->toString(), 'some string');
    }

    public function testCreate()
    {
        $buffer = StringBuffer::create('STRING');
        $this->assertInstanceOf(StringBuffer::class, $buffer);
        $this->assertSame($buffer->toString(), 'STRING');
    }

    public function testPrepend(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->prepend('before-');
        $this->assertSame('before-STRING', $buffer->toString());
    }

    public function testAppend(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->append('-after');
        $this->assertSame('STRING-after', $buffer->toString());
    }

    public function testAppendFormatted(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->appendFormatted('-%s-%s', 'a', 'b');
        $this->assertSame('STRING-a-b', $buffer->toString());
    }

    public function testPrependFormatted(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->prependFormatted('%s-%s-', 'a', 'b');
        $this->assertSame('a-b-STRING', $buffer->toString());
    }

    public function testAppendIf(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->appendIf(true, '-true', '-false');
        $this->assertSame('STRING-true', $buffer->toString());

        $buffer = new StringBuffer('STRING');
        $buffer->appendIf(false, '-true', '-false');
        $this->assertSame('STRING-false', $buffer->toString());
    }

    public function testPrependIf(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->prependIf(true, 'true-', 'false-');
        $this->assertSame('true-STRING', $buffer->toString());

        $buffer = new StringBuffer('STRING');
        $buffer->prependIf(false, 'true-', 'false-');
        $this->assertSame('false-STRING', $buffer->toString());
    }

    public function testAppendIfNot(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->appendIfNot(false, '-true', '-false');
        $this->assertSame('STRING-true', $buffer->toString());

        $buffer = new StringBuffer('STRING');
        $buffer->appendIfNot(true, '-true', '-false');
        $this->assertSame('STRING-false', $buffer->toString());
    }

    public function testPrependIfNot(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->prependIfNot(false, 'true-', 'false-');
        $this->assertSame('true-STRING', $buffer->toString());

        $buffer = new StringBuffer('STRING');
        $buffer->prependIfNot(true, 'true-', 'false-');
        $this->assertSame('false-STRING', $buffer->toString());
    }

    public function testAppendIfNull(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->appendIfNull(null, '-true', '-false');
        $this->assertSame('STRING-true', $buffer->toString());

        $buffer = new StringBuffer('STRING');
        $buffer->appendIfNull('not null', '-true', '-false');
        $this->assertSame('STRING-false', $buffer->toString());
    }

    public function testPrependIfNull(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->prependIfNull(null, 'true-', 'false-');
        $this->assertSame('true-STRING', $buffer->toString());

        $buffer = new StringBuffer('STRING');
        $buffer->prependIfNull('not null', 'true-', 'false-');
        $this->assertSame('false-STRING', $buffer->toString());
    }

    public function testAppendIfNotNull(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->appendIfNotNull('not null', '-true', '-false');
        $this->assertSame('STRING-true', $buffer->toString());

        $buffer = new StringBuffer('STRING');
        $buffer->appendIfNotNull(null, '-true', '-false');
        $this->assertSame('STRING-false', $buffer->toString());
    }

    public function testPrependIfNotNull(): void
    {
        $buffer = new StringBuffer('STRING');
        $buffer->prependIfNotNull('not null', 'true-', 'false-');
        $this->assertSame('true-STRING', $buffer->toString());

        $buffer = new StringBuffer('STRING');
        $buffer->prependIfNotNull(null, 'true-', 'false-');
        $this->assertSame('false-STRING', $buffer->toString());
    }

    public function testAppendImplode(): void
    {
        $buffer = new StringBuffer('STRING-');
        $buffer->appendImplode(['a', 'b', 'c'], ' ');
        $this->assertSame('STRING-a b c', $buffer->toString());

        $buffer = new StringBuffer('STRING-');
        $buffer->appendImplode(['a', 'b', 'c'], ',');
        $this->assertSame('STRING-a,b,c', $buffer->toString());
    }

    public function testPrependImplode(): void
    {
        $buffer = new StringBuffer('-STRING');
        $buffer->prependImplode(['a', 'b', 'c'], ' ');
        $this->assertSame('a b c-STRING', $buffer->toString());

        $buffer = new StringBuffer('-STRING');
        $buffer->prependImplode(['a', 'b', 'c'], ',');
        $this->assertSame('a,b,c-STRING', $buffer->toString());
    }

    public function testToString(): void
    {
        $buffer = new StringBuffer('STRING');
        $this->assertSame('STRING', $buffer->toString());
        $this->assertSame('STRING', (string) $buffer);
    }
}